//
//  RootNavigationController.swift
//  EmployeeList
//
//  Created by Aziza  on 05.05.21.
//

import UIKit

class RootNavigationController: UINavigationController {
    
    var controller: UIViewController
    
    override init(rootViewController: UIViewController) {
        controller = rootViewController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewControllers = [controller]
    }
}
