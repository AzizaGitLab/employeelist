//
//  ContactsDetailView.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import UIKit
import Contacts

class EmployeeDetailController: UIViewController {
    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var iphoneNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var projectsTableView: UITableView!
    
    var item: EmployeeListItemCellVM? = nil
    private var navigator: ContactNavigator?
    private var contactViewModel = ContactsViewModel()
    
    var contact: Friend{
        let name = item?.name ?? ""
        let surName = item?.surName ?? ""
        let phoneNumber = item?.phoneNumber ?? ""
        let email = item?.email ?? ""
        return Friend(firstName: name, lastName: surName, workEmail: email, phoneNumber: phoneNumber)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindData()
        setupNavigation()
        projectsTableView.reloadData()
        navigator = ContactNavigator(navigationController: self.navigationController,
                                     vcFactory: ContactNavigatorFactory())
        self.projectsTableView.tableFooterView = UIView(frame: .zero)
    }
    
    private func bindData(){
        self.fullName.text = item?.name ?? ""
        self.iphoneNumber.text = item?.phoneNumber
        self.position.text = item?.position
        self.email .text = item?.email
    }
    
    // if user exists on db show icon which leads to native contact detail screen
    private func setupNavigation(){
        self.navigationItem.title = "Employee"
       // if contactViewModel.checkBuiltIntContacts(contact){
            let addIcon = UIImage(named: "activeContactIcon")
            let addItem = UIBarButtonItem(image: addIcon,
                                          style: .plain,
                                          target: self,
                                          action: #selector(openContact))
            self.navigationItem.rightBarButtonItems = [addItem]
        //}
    }
    
    @objc private func openContact(){
        navigator?.push(to: .contact(contact))
    }
}

extension EmployeeDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item?.projects.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProjectCell.self), for: indexPath) as? ProjectCell else { return UITableViewCell() }
        cell.configure(item?.projects[indexPath.row] ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Projects"
    }
}

