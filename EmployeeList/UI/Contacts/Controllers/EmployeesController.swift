//
//  EmployeeListController.swift
//  EmployeeList
//
//  Created by Aziza  on 05.05.21.
//

import UIKit

class EmployeesController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var employeeListTableView: UITableView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    private var navigator: ContactNavigator?
    private lazy var viewModel = EmployeesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        employeeListTableView.setupPullToRefresh {
            self.getData()
        }
        navigator = ContactNavigator(navigationController: self.navigationController,
                                     vcFactory: ContactNavigatorFactory())
        self.navigationItem.title = "Employees List"
        
        getData()
        hideKeyboardWhenTappedAround()
        employeeListTableView.tableFooterView = UIView(frame: .zero)
    }
    
    func getData() {
        let loadingView = UIViewController.displaySpinner(onView: self.view)
        viewModel.fetch { [weak self] success in
            UIViewController.removeSpinner(spinner: loadingView)
            self?.employeeListTableView.pullToRefreshView.stopAnimating()
            self?.setSegmentedControl()
            self?.updateList()
        }
    }
    
    private func setSegmentedControl(){
        let groupItems = Array(Set(viewModel.groups))
        segmentedControl.replaceSegments(segments: groupItems)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: UIControl.State.selected)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(updateList), for: .valueChanged)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.apportionsSegmentWidthsByContent = true
    }
    
    @objc private func updateList(){
        let index = segmentedControl.selectedSegmentIndex
        guard let title = segmentedControl.titleForSegment(at: index) else {
            return
        }
        viewModel.filterList(title) { filtered in
            if filtered {
                self.employeeListTableView.reloadData()
            }
        }
    }
}

extension EmployeesController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.currentCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EmployeeItemCell.self), for: indexPath) as? EmployeeItemCell else { return UITableViewCell() }
        if indexPath.row < viewModel.currentCount {
            cell.configure(viewModel.getEmployeeListItem(at: indexPath.row))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigator?.push(to: .employeeDetail(viewModel.getEmployeeListItem(at: indexPath.row)))
    }
}


extension EmployeesController:  UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard let fieldText = searchBar.text else { return }
        viewModel.filterList(fieldText) { (filtered) in
            self.employeeListTableView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            getData()
            searchBar.endEditing(true)
            self.view.endEditing(true)
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

extension UISegmentedControl {
    func replaceSegments(segments: Array<String>) {
        self.removeAllSegments()
        for segment in segments {
            self.insertSegment(withTitle: segment, at: self.numberOfSegments, animated: false)
        }
    }
}

