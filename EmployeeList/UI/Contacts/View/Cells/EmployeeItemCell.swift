//
//  EmployeeItemCell.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import UIKit

class EmployeeItemCell: UITableViewCell {
    
    @IBOutlet weak var employeeFullName: UILabel!
    
    var viewModel: EmployeeListItemCellVM?

    // MARK: -Set Data
    func configure(_ viewModel: EmployeeListItemCellVM) {
        self.viewModel = viewModel
        employeeFullName.text = viewModel.name + " " + viewModel.surName
        
    }
}
