//
//  ProjectCell.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/4/21.
//

import UIKit

class ProjectCell: UITableViewCell {

    @IBOutlet weak var projectNameLabel: UILabel!
    
    // MARK: -Set Data
    func configure(_ project: String) {
        projectNameLabel.text = project
    }
}
