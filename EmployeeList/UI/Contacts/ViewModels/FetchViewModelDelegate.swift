//
//  FetchViewModelDelegate.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/4/21.
//

import Foundation

protocol FetchViewModelDelegate: class {
  func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
  func onFetchFailed(with reason: String)
}
