//
//  EmployeesViewModel.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/4/21.
//

import Foundation

class EmployeesViewModel {
    
    typealias Response = (ErrorService?) -> Void
    private var fetch = 10
    private var offset = 0
    private var total = 0
    private var isFetchInProgress = false
    var refreshing: Box = Box(false)
    var employees: [EmployeeModel] = []
    var filteredList: [EmployeeModel] = []
    var groups: [String] = []
    let group = DispatchGroup()
    
    
    var totalCount: Int {
        return total
    }
    
    var currentCount: Int {
        return filteredList.count
    }
    
    
    func fetch(completion:  @escaping Response){
        guard !isFetchInProgress else { return }
        isFetchInProgress = true
        refreshing.value = true
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        
        EmployeeApiService.shared.getFirstList() {[weak self] result in
            guard let self = self else { return }
            self.refreshing.value = false
            switch result {
            case .failure(let error):
                self.isFetchInProgress = false
                completion(error)
                
            case .success(let data):
                DispatchQueue.main.async {
                    guard let employeeItems = data?.employees else { return }
                    self.employees.append(contentsOf: employeeItems)
                    dispatchGroup.leave()
                }
            }
        }
        
        dispatchGroup.enter()
        EmployeeApiService.shared.getSecondList() {[weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(error)
                
            case .success(let data):
                DispatchQueue.main.async {
                    self.isFetchInProgress = false
                    guard let employeeItems = data?.employees else { return }
                    self.employees.append(contentsOf: employeeItems)
                    self.total = self.employees.count
                    dispatchGroup.leave()
                }
                
                dispatchGroup.notify(queue: .main) {
                    for item in self.employees {
                        guard let positionName = item.position else {return}
                        self.groups.append(positionName)
                    }
                    
                    self.employees.sort(by: {
                                            guard let first: String = ($0 as EmployeeModel).fname else { return false }
                                            guard let second: String = ($1 as EmployeeModel).fname else { return true }
                                            return first < second})
                    
                    
                    completion(.none)
                }
            }
        }
    }
    
    func filterList(_ title: String, completion: @escaping(Bool) -> ()){
        filteredList.removeAll()
        filteredList = employees.filter { item in
            return item.position == title || item.fname == title || item.lname == title || item.contact_details?.email == title
        }
        completion(true)
    }
    
    // Returns the object at the given index path
    func getEmployeeListItem(at index: Int) -> EmployeeListItemCellVM {
        let employeeItem = filteredList[index]
        return EmployeeListItemCellVM(item: employeeItem)
    }
    
    private func calculateIndexPathsToReload(from newList: [EmployeeModel]) -> [IndexPath] {
        let startIndex = employees.count - newList.count
        let endIndex = startIndex + newList.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}
