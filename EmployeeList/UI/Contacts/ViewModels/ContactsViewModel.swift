//
//  File.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/4/21.
//

import Foundation
import Contacts

class ContactsViewModel {
    let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
    
    func checkBuiltIntContacts(_ contact: Friend) -> Bool{
        var contacts = [CNContact]()
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        let request = CNContactFetchRequest(keysToFetch:  keys as [CNKeyDescriptor])
        
        let contactStore = CNContactStore()
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                contacts.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }
        return contacts.contains(contact.contactValue)
    }
 }
