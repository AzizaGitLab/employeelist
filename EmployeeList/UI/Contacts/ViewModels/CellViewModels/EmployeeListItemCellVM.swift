//
//  ContactListItemCellVM.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Foundation

class EmployeeListItemCellVM {
    private var item: EmployeeModel
    
    init(item: EmployeeModel) {
        self.item = item
    }
    
    var name: String{
        return item.fname ?? ""
    }
    
    var surName: String{
        return item.lname ?? ""
    }
    
    var phoneNumber: String{
        return item.contact_details?.phone ?? "XX-XX-XX"
    }
    
    var email: String{
        return item.contact_details?.email ?? "XXXX"
    }
    
    var position: String{
        return item.position ?? ""
    }
    
    var projects: [String]{
        return item.projects ?? [String]()
    }
}
