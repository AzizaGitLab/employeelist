//
//  Extensions + UIViewController.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/4/21.
//

import UIKit
import SwiftMessages

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.8)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.color = UIColor.init(named: "appColor")
        ai.startAnimating()
        ai.center = spinnerView.center
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    /**
    * Function for Showing alert(message) with - SwiftMessages
    */
    func showAlert(type: Theme = .error, body: String) {
        let alertID = "notificationAlert"
        SwiftMessages.hide(id: alertID)
        let errorMessage = MessageView.viewFromNib(layout: .messageView)
        errorMessage.configureTheme(type)
        errorMessage.id = alertID
        errorMessage.button?.isHidden = true
        errorMessage.configureContent(body: body)
        errorMessage.titleLabel?.isHidden = true
        
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        DispatchQueue.main.async {
            SwiftMessages.show(config: config, view: errorMessage)
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
