//
//  Extensions + UITableView.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/4/21.
//

import Foundation
import SVPullToRefresh

extension UITableView {
    func setupPullToRefresh(onComplete: @escaping () -> () ) {
        addPullToRefresh {
            onComplete()
        }
    }
}
