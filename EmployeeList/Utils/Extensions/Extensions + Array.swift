//
//  Extensions + Array.swift
//  EmployeeList
//
//  Created by Aziza  on 06.05.21.
//

import Foundation

extension Array where Element: Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            guard !uniqueValues.contains(item) else { return }
            uniqueValues.append(item)
        }
        return uniqueValues
    }
}

extension Array where Element: StringProtocol {
    public func caseInsensitiveSorted(_ result: ComparisonResult) -> [Element] {
            sorted { $0.caseInsensitiveCompare($1) == result }
        }
}
