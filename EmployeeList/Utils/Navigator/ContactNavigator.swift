//
//  ContactNavigator.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import UIKit

class ContactNavigator: NavigatorManager {
    private weak var navigationController: UINavigationController?
    private let vcFactory: ContactNavigatorFactory
    
    init(navigationController: UINavigationController?, vcFactory: ContactNavigatorFactory) {
        self.vcFactory = vcFactory
        self.navigationController = navigationController
    }
    
    func push(to destination: Destination) {
        navigationController?.pushViewController(makeViewController(for: destination), animated: false)
    }
    
    func present(destination: Destination) {
        let controller = makeViewController(for: destination)
        if #available(iOS 13.0, *) {
            controller.modalPresentationStyle = .fullScreen
        }
        navigationController?.present(controller, animated: false)
    }
    
    enum Destination {
        case employessList
        case employeeDetail(_ item: EmployeeListItemCellVM)
        case contact(_ contact: Friend)
    }
    
    func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
        case .employessList:
            return vcFactory.employeesList()
        case .employeeDetail(let item):
            return vcFactory.employeeDetail(item)
        case .contact(let contact):
            return vcFactory.openContact(contact)
        }
    }
}


