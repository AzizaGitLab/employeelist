//
//  NavigatorManager.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import UIKit


protocol NavigatorManager {
    associatedtype Destination
    func makeViewController(for destination: Destination) -> UIViewController
}
class NavigatorFactory {
    func instantiate<T: UIViewController>(_ viewController: T.Type = T.self,
                                          storyboard: String,
                                          bundle: Bundle? = nil) -> T {
        let storyboard = UIStoryboard(name: storyboard, bundle: bundle)
        guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: viewController)) as? T else {
            fatalError("Can't instantiate viewController with identifier \(String(describing: viewController))")
        }
        return vc
    }
}
