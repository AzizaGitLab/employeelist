//
//  NavigatorFactory.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import UIKit
import ContactsUI

class ContactNavigatorFactory {
    
    static let instance = ContactNavigatorFactory()
    
    private func getStoryboard(with name: Storyboards) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
    
    func employeesList() -> EmployeesController {
        return  getStoryboard(with: .employee).instantiateViewController(withIdentifier: "EmployeesController") as! EmployeesController
    }
    
    func employeeDetail(_ employeeItem: EmployeeListItemCellVM) -> EmployeeDetailController {
        let vc = getStoryboard(with: .employee).instantiateViewController(withIdentifier: "EmployeeDetailController") as! EmployeeDetailController
        vc.item = employeeItem
        return vc
    }
    
    func openContact(_ friend: Friend) -> CNContactViewController{
        // 1
        
        let contact = friend.contactValue
        // 2
        let contactViewController = CNContactViewController(forUnknownContact: contact)
        contactViewController.hidesBottomBarWhenPushed = true
        contactViewController.allowsEditing = false
        contactViewController.allowsActions = false
        // 3
        //navigationController?.navigationBar.tintColor = .appBlue
 
        return contactViewController

    }
    
    func getRoot() -> RootNavigationController {
        return getStoryboard(with: .employee).instantiateViewController(withIdentifier: "Root") as! RootNavigationController
    }
}


