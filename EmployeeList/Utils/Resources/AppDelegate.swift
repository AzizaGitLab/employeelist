//
//  AppDelegate.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import UIKit
import Foundation
import Contacts

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setRootController()
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(named: "appColor")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        CNContactStore().requestAccess(for: .contacts) { (access, error) in
            print("Access: \(access)")
        }
        return true
    }
    
    func setRootController(){
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.rootViewController = ContactNavigatorFactory.instance.employeesList()
        self.window?.makeKeyAndVisible()
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

