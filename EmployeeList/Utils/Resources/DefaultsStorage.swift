//
//  DefaultsStorage.swift
//  EmployeeList
//
//  Created by Aziza  on 07.05.21.
//

import Foundation
import UIKit

enum DefaultsStorageKey: String {
    case contactList  = "contactList"
   
}

class DefaultsStorage {
    
    // MARK: - UIImage
    
    class func set(pngImage: UIImage, by key: DefaultsStorageKey) {
        let data = pngImage.pngData()
        UserDefaults.standard.set(data, forKey: key.rawValue)
    }
    
    class func set(jpgImage: UIImage, by key: DefaultsStorageKey) {
        let data = jpgImage.jpegData(compressionQuality: 1.0)
        UserDefaults.standard.set(data, forKey: key.rawValue)
    }
    
    class func getImage(by key: DefaultsStorageKey) -> UIImage? {
        guard let data  = UserDefaults.standard.data(forKey: key.rawValue) else { return nil }
        guard let image = UIImage(data: data) else { return nil }
        return image
    }
    
    // MARK: - String
    
    class func set(string: String, by key: DefaultsStorageKey) {
        UserDefaults.standard.set(string, forKey: key.rawValue)
    }
    
    class func getString(by key: DefaultsStorageKey) -> String? {
        guard let string = UserDefaults.standard.string(forKey: key.rawValue) else { return nil }
        return string
    }
    
    // MARK: - Int
    
    class func set(int: Int, by key: DefaultsStorageKey) {
        UserDefaults.standard.set(int, forKey: key.rawValue)
    }
    
    class func getInteger(by key: DefaultsStorageKey) -> Int {
        let int = UserDefaults.standard.integer(forKey: key.rawValue)
        return int
    }
    
    // MARK: - Float
    
    class func set(float: Float, by key: DefaultsStorageKey) {
        UserDefaults.standard.set(float, forKey: key.rawValue)
    }
    
    class func getFloat(by key: DefaultsStorageKey) -> Float {
        let float = UserDefaults.standard.float(forKey: key.rawValue)
        return float
    }
    
    // MARK: - Bool
    
    class func set(bool: Bool, by key: DefaultsStorageKey) {
        UserDefaults.standard.set(bool, forKey: key.rawValue)
    }
    
    class func getBool(by key: DefaultsStorageKey) -> Bool {
        let bool = UserDefaults.standard.bool(forKey: key.rawValue)
        return bool
    }
    
    // MARK: - Data
    
    class func set<T: Codable>(entity: T, by key: DefaultsStorageKey) {
        if let encoded = try? JSONEncoder().encode(entity) {
            UserDefaults.standard.set(encoded, forKey: key.rawValue)
        }
    }
    
    class func getEntity<T: Codable>(by key: DefaultsStorageKey) -> T? {
        guard let data = UserDefaults.standard.value(forKey: key.rawValue) as? Data else { return nil }
        let entity = try? JSONDecoder().decode(T.self, from: data)
        return entity
    }
    
    // MARK: - Delete
    
    class func delete(by key: DefaultsStorageKey) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
}
