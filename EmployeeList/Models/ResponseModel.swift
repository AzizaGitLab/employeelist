//
//  ResponseModel.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

struct ResponseModel<T: Decodable>: Decodable {
    var appName: String?
    var data: T?
    var description: String?
    var exception: Exeption?
    var status: Int?
    var timestamp: String?
    var transaction: String?
}


struct SuccessModel: Decodable {
    var success: Bool?
    var error: String?
    var message: String?
    var code: String?
}
