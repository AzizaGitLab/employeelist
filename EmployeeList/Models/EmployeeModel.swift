//
//  EmployeeModel.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Foundation

struct EmployeeModel: Decodable {
    var fname: String?
    var lname: String?
    var position: String?
    var contact_details: ContactDetailModel?
    var projects: [String]?
}
