//
//  ExceptionModel.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

struct Exeption: Decodable {
    var code: Int?
    var errorStack: String?
    var errors: [Errors]?
    var message: String?
}

struct Errors: Decodable {
    var key: String?
    var message: String?
}
