//
//  ContactsModel.swift
//  EmployeeList
//
//  Created by Aziza Jabrailova on 5/6/21.
//

import UIKit
import Contacts

class Friend {
  var firstName: String
  var lastName: String
  var workEmail: String
  var identifier: String?
  var storedContact: CNMutableContact?
  var phoneNumberField: (CNLabeledValue<CNPhoneNumber>)?
  
    init(firstName: String, lastName: String, workEmail: String, phoneNumber: String){
    self.firstName = firstName
    self.lastName = lastName
    self.workEmail = workEmail
    self.phoneNumberField = CNLabeledValue(
                   label:CNLabelPhoneNumberiPhone,
                   value:CNPhoneNumber(stringValue: phoneNumber))
  }
}

extension Friend: Equatable {
  static func ==(lhs: Friend, rhs: Friend) -> Bool{
    return lhs.firstName == rhs.firstName &&
      lhs.lastName == rhs.lastName &&
      lhs.workEmail == rhs.workEmail
  }
}

extension Friend {
  var contactValue: CNContact {
    let contact = CNMutableContact()
    contact.givenName = firstName
    contact.familyName = lastName
    if let phoneNumberField = phoneNumberField {
      contact.phoneNumbers.append(phoneNumberField)
    }
    contact.emailAddresses = [CNLabeledValue(label: CNLabelWork, value: workEmail as NSString)]
    return contact.copy() as! CNContact
  }
}
