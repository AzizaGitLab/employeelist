//
//  ContactDetailModel.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Foundation

struct ContactDetailModel: Decodable {
    var email: String?
    var phone: String?
}
