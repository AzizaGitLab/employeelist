//
//  Pagination.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

struct Pagination<T:Decodable>:Decodable{
    var employees: [T]?
}
