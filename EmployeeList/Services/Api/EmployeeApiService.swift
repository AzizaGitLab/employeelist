//
//  ContactApiService.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Foundation

class EmployeeApiService: APIService {
    static let shared = EmployeeApiService()
    
    func getFirstList(completion: @escaping ResponseResult<Pagination<EmployeeModel>?>){
        request(to: Router.firstList, responseType: .simple,completion: completion)
    }
    
    func getSecondList(completion: @escaping ResponseResult<Pagination<EmployeeModel>?>){
        request(to: Router.secondList, responseType: .simple,completion: completion)
    }
    
}
