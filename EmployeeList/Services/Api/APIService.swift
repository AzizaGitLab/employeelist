//
//  APIService.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Foundation
import Alamofire


class APIService {
    typealias ResponseResult<T: Decodable> = (Swift.Result<T, ErrorService>) -> Void
    
    private var Manager : Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 90
        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()
    
    func request<T: Decodable>(to route: APIConfiguration, responseType: ResponseType = .structurized,completion: @escaping (Swift.Result<T?, ErrorService>) -> Void) {
        Manager.request(route).validate().responseJSON { response in
            let decoder = JSONDecoder()
            let baseResponse = decoder.decodeResponse(T.self, response: response, responseType: responseType)
            completion(baseResponse)
        }
     }
}

extension JSONDecoder {
    func decodeResponse<T: Decodable>(_ type: T.Type, response: DataResponse<Any>, responseType: ResponseType = .structurized) -> Swift.Result<T?, ErrorService> {
        switch response.result {
        case .failure(let error):
            if error._code == NSURLErrorTimedOut {
                return .failure(.timeOutError)
            }
            if error._code == NSURLErrorNotConnectedToInternet {
               return .failure(.connectionError)
            }
            if let httpStatusCode = response.response?.statusCode {
                 return .failure(checkError(httpStatusCode))
            }
            
          return .failure(.emptyResponseError)
            
        case .success(_):
            guard let data = response.data
                else { return .success(nil) }
                nonConformingFloatDecodingStrategy = .throw
                do {
                    guard responseType == .structurized else {
                        let result = try decode(T.self, from: data)
                        return .success(result)
                    }
                      
                    let result = try decode(ResponseModel<T>.self, from: data)
                    guard result.status == 200 else{
                        let exceptionCode = result.exception?.code
                        if result.status == 401 {
                            return .failure(checkError(result.status, exceptionCode))
                        }
                        return .failure(checkError(result.status, exceptionCode))
                    }
                    guard let data = result.data else { return .success(nil) }
                    return .success(data)
                    
                } catch let error {
                    print("error", error)
                    return .failure(.parseError)
                }
         }
    }
    
    private func checkError(_ statusCode: Int?, _ excCode: Int? = nil) -> ErrorService {
        switch statusCode {
        case 401:
            return .unauthorized
        case 400:
            return .badRequest(excCode)
        case 403:
            return .notAllowed(excCode)
        case 404:
            return .pageNotFound(excCode)
        case 409:
            return .alreadyAdded(excCode)
        case 500:
            return .serverError
        default:
            return .noData
        }
    }
}

enum ResponseType {
    case simple
    case structurized
}
