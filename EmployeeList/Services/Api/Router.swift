//
//  Router.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Alamofire

enum Router: APIConfiguration {
    var baseURL: String{
        switch self {
        case .firstList:
            return "https://tartu-jobapp.aw.ee"
        case .secondList:
            return "https://tallinn-jobapp.aw.ee"
        }
    }
    
    var mainPath: String {
        switch self {
        case .firstList,
             .secondList:
            return "/employee_list/"
        }
    }
    
    case firstList
    case secondList
    
    var method: HTTPMethod {
        switch self {
        case .firstList,
             .secondList:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .firstList,
             .secondList:
            return ""
        }
    }
    
    var header: [String: String] {
        switch self {
        default:
            return [HTTPHeaderField.contentType.rawValue: ContentType.json.rawValue]
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .firstList,
             .secondList:
            return nil
        }
    }
    
    var body: String? {
        switch self {
        default: return nil
        }
    }
}
