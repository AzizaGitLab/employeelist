//
//  APIConfiguration.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Alamofire

protocol APIConfiguration: URLRequestConvertible {
    var baseURL: String {get}
    var mainPath: String { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var header: [String: String] { get }
    var parameters: Parameters? { get }
    var body: String? { get }
}

struct MainUrlConfiguration {
    static let baseUrl = APIDomain.prod.rawValue
}


extension APIConfiguration {
    func asURLRequest() throws -> URLRequest {
        let urlPath = baseURL + mainPath + path
        guard let url = URL(string: urlPath) else { throw ErrorService.incorrectUrl }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 90)
        
        // HTTP Method
        request.httpMethod = method.rawValue
        
        //Headers
        request.allHTTPHeaderFields = header
        
        //Body
        request.httpBody = body?.data(using: .utf8, allowLossyConversion: false)
        
        
        // Parameters
        if let parameters = parameters {
            do{
                request = try URLEncoding.default.encode(request, with: parameters)
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        return request
    }
}

enum APIDomain: String {
    case prod = "https://tartu-jobapp.aw.ee"
    case dev = "https://tallinn-jobapp.aw.ee"
  
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case request_source = "request-source"
    case request_number = "request-number"
}

enum ContentType: String {
    case json = "application/json"
    case multipart = "multipart/form-data"
}
