//
//  ErrorService.swift
//  EmployeeList
//
//  Created by Aziza  on 04.05.21.
//

import Foundation

enum ErrorService: Error, LocalizedError, Equatable {
    case noData,
        badRequest(Int?),
        fileNotFound(String),
        parseError,
        emptyResponseError,
        pageNotFound(Int?),
        serverError,
        incorrectUrl,
        dataSaveError,
        dataDeleteError,
        connectionError,
        timeOutError,
        unauthorized,
        notAllowed(Int?),
        alreadyAdded(Int?),
        defaultError(String)

    var exceptionCode: Int? {
        switch self {
        case .badRequest(let code),
             .pageNotFound(let code),
             .alreadyAdded(let code),
             .notAllowed(let code):
            return code
        default:
            return nil
        }
    }
    
    var errorDescription: String? {
        switch self {
        case .noData:
            return "Could not fetch data from server or local storage!"
        case .badRequest:
            return "Bad request"
        case .fileNotFound(let fileName):
            return "Could not find file named as \(fileName)"
        case .parseError:
            return "Error occured while parsing data!"
        case .emptyResponseError:
            return "Response is empty!"
        case .pageNotFound:
            return "404: Page not found or incorrect url!"
        case .serverError:
            return "Error occured while connecting to server!"
        case .incorrectUrl:
            return "Request URL is incorrect!"
        case .dataSaveError:
            return "Error while data saving!"
        case .dataDeleteError:
            return "Error while data deleting!"
        case .timeOutError:
            return "Error time out"
        case .unauthorized:
            return "Unauthorized accessing"
        case .notAllowed:
            return "Not allowed"
        case .alreadyAdded:
            return "Already added"
        case .connectionError:
            return "The Internet connection appears to be offline."
        case .defaultError(let errorMessage):
            return errorMessage
        }
    }
}
